#include <iostream>
#include <memory>

#include "Registry.h"

// Note: we use a different namespace to test if the macros defined in
// Registry.h actuall works with a different namespace from c10.
namespace c10_test {

	class Foo {
	public:
		explicit Foo(int x) {
			std::cout << "Foo " << x;
		}
		virtual ~Foo() {}
	};

	C10_DECLARE_REGISTRY(FooRegistry, Foo, int);
	C10_DEFINE_REGISTRY(FooRegistry, Foo, int);
#define REGISTER_FOO(clsname) C10_REGISTER_CLASS(FooRegistry, clsname, clsname)

	class Bar : public Foo {
	public:
		explicit Bar(int x) : Foo(x) {
			std::cout << "Bar " << x;
		}
	};
	REGISTER_FOO(Bar);

	class AnotherBar : public Foo {
	public:
		explicit AnotherBar(int x) : Foo(x) {
			std::cout << "AnotherBar " << x;
		}
	};
	REGISTER_FOO(AnotherBar);

	// C10_REGISTER_CLASS_WITH_PRIORITY defines static variable
	void RegisterFooDefault() {
		C10_REGISTER_CLASS_WITH_PRIORITY(
			FooRegistry, FooWithPriority, c10::REGISTRY_DEFAULT, Foo);
	}

	void RegisterFooDefaultAgain() {
		C10_REGISTER_CLASS_WITH_PRIORITY(
			FooRegistry, FooWithPriority, c10::REGISTRY_DEFAULT, Foo);
	}

	void RegisterFooBarFallback() {
		C10_REGISTER_CLASS_WITH_PRIORITY(
			FooRegistry, FooWithPriority, c10::REGISTRY_FALLBACK, Bar);
	}

	void RegisterFooBarPreferred() {
		C10_REGISTER_CLASS_WITH_PRIORITY(
			FooRegistry, FooWithPriority, c10::REGISTRY_PREFERRED, Bar);
	}
} // namespace c10_test

int main(int argc, char **argv) {
	std::unique_ptr<c10_test::Foo> bar(c10_test::FooRegistry()->Create("Bar", 1));
	std::unique_ptr<c10_test::Foo> another_bar(c10_test::FooRegistry()->Create("AnotherBar", 1));
	return 0;
}