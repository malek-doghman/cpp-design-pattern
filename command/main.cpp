#include <iostream>
#include <string>
#include <list>

using namespace std;

class ICommand
{
public:
	ICommand() {}
	virtual ~ICommand() {}
	virtual void Execute() = 0;
};

class CommandExecutor
{
public:
	CommandExecutor() {}
	virtual ~CommandExecutor() {}
	void StoreAndExecute(ICommand* command) {
		commandList.push_back(command);
		command->Execute();
	}
private:
	list<ICommand*> commandList;
};

class Warrior
{
public:
	Warrior() {}
	~Warrior() {}
	void Jump() {
		cout << "warrior Jumping yoopi !!!" << endl;

	}
	void Attack() {
		cout << "Dragon Heavy Strike" << endl;
	}
};

class Mage
{
public:
	Mage() {}
	~Mage() {}
	void Jump() {
		cout << "Mage Flying hhhhhooo !!!" << endl;
	}
	void Attack() {
		cout << "fierce Fire BALL" << endl;
	}
};

template <class T>
class AttackCommand : public ICommand
{
public:
	AttackCommand(T _w) {
		W = _w;
	}
	~AttackCommand() {}
	void Execute() {
		W.Attack();
	}
private:
	T W;
};

template <class T>
class JumpCommand : public ICommand
{
public:
	JumpCommand(T _w) {
		W = _w;
	}
	~JumpCommand() {}
	void Execute() {
		W.Jump();
	}
private:
	T W;
};


int main() {
	Warrior w;
	Mage m;
	JumpCommand<Warrior> j(w);
	JumpCommand<Mage> j1(m);
	AttackCommand<Mage> a(m);
	AttackCommand<Warrior> a2(w);

	CommandExecutor c;

	ICommand* aaa = &j;
	aaa->Execute();
	aaa = &a;
	aaa->Execute();

	c.StoreAndExecute(&a);
	c.StoreAndExecute(&a2);

	c.StoreAndExecute(&j);
	c.StoreAndExecute(&j1);
	return 0;
}