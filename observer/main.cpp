#include <string>
#include <set>
#include <iostream>
using namespace std;

class IObserver
{
public:
	virtual void update(string data) = 0;
};

class Observable
{
private:
	set<IObserver*> list_observers;

public:
	void notify(string data) const
	{
		// Notifier tous les observers
		for (set<IObserver*>::const_iterator it = list_observers.begin();
			it != list_observers.end(); ++it)
			(*it)->update(data);
	}

	void addObserver(IObserver* observer)
	{
		// Ajouter un observer a la liste
		list_observers.insert(observer);
	}

	void removeObserver(IObserver* observer)
	{
		// Enlever un observer a la liste
		list_observers.erase(observer);
	}
};

class Display : public IObserver
{
	void update(string data)
	{
		cout << "Evenement : " << data << endl;
	}
};

class Exemple : public Observable
{
public:
	void message(string message)
	{
		// Lancer un evenement lors de la reception d'un message
		notify(message);
	}
};

int main()
{
	Display display;
	Exemple exemple;

	// On veut que "Display" soit pr�venu � chaque r�ception d'un message dans "Exemple"
	exemple.addObserver(&display);
	// On envoie un message a Exemple
	exemple.message("reception d'un message"); // Sera affich� par Display

	return 0;
}