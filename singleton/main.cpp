#include <iostream>
#include <string>
#include <list>

//is this thread safe ? 
//yes it is after c++ 11 
template <class T>
class Singleton {
public:
	static T& GetInstance() {
		static T theSingleInstance;
		return theSingleInstance;
	}
};

class Log : public Singleton<Log> {
public:

	friend class Singleton<Log>;

	static void Write(std::string s) {
		m_data.push_back(s);
	}

private:
	Log() {}        // ctor is hidden
	Log(Log const&) {}    // copy ctor is hidden
	Log& operator=(Log const&) {}  // assign op is hidden

	//we can remove the static keyword
	static std::list<std::string> m_data;
};

std::list<std::string> Log::m_data;

int main() {
	Log& l = Log::GetInstance();
	l.Write("message1");
	Log::Write("message2");
	Log::GetInstance().Write("message3");

	return 0;
}