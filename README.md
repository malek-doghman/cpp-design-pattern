# cpp-design-pattern
Implementation of some design pattern in c++.

  - Singleton 
  - Prototype
  - Observer
  - Command
  - registry (from https://github.com/pytorch/pytorch/blob/master/c10/util/Registry.h)

This work was inspired from the excellent online book by Bob Nystrom:
http://gameprogrammingpatterns.com/