#include <iostream>
#include <string>

class Monster
{
public:
	Monster(int health, int speed) : health_(health), speed_(speed) {}
	virtual ~Monster() {}
	virtual Monster* clone() { return new Monster(health_, speed_); }
	virtual float Attack() { return 0.0f; }
protected:
	int health_;
	int speed_;
};

class Ghost : public Monster
{
public:
	Ghost(int health, int speed, int power) : Monster(health, speed), power_(power) {}
	~Ghost() {}
	Monster* clone() {
		return new Ghost(health_, speed_, power_);
	}
	virtual float Attack() { return power_ * speed_; }
private:
	int power_;
};

Monster* spawnGhost()
{
	return new Ghost(150, 100, 20);
}

typedef Monster* (*SpawnCallback)();

class Spawner
{
public:
	Spawner(SpawnCallback spawn)
		: spawn_(spawn)
	{}

	Monster* spawnMonster()
	{
		return spawn_();
	}

private:
	SpawnCallback spawn_;
};

int main() {
	Spawner* spawner = new Spawner(spawnGhost);
	Monster * monster = spawner->spawnMonster();
	std::cout << "Monster attack value = " << monster->Attack() << std::endl;
	delete monster;
	delete spawner;
	return 0;
}